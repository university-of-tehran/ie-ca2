package handlers;

import java.io.File;
import java.io.IOException;
import java.util.List;
import io.javalin.http.Context;
import models.Grade;
import models.Student;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import utils.Utils;
import models.Course;

import static handlers.Constants.*;

public class GetProfileDetailsHandler {
    public static void handle(Context ctx) {
        List<Student> students = Utils.getStudentsFromFile();
        List<Grade> grades = Utils.getStudentGradesFromFile(ctx.pathParam("student_id"));
        List<Course> courses = Utils.getCoursesFromFile();

        Student student = null;
        for (Student s: students) {
            if (s.id.equals(ctx.pathParam("student_id"))) {
                student = s;
            }
        }

        if (student == null) {
            ctx.html("Not found.");
            ctx.status(404);
            return;
        }

        Document doc = null;
        try {
            String fileName = PROFILE_TEMPLATE;
            doc = Jsoup.parse(new File(fileName), "utf-8");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e.toString());
        }
        Element table = doc.select("table").first();

        int totalPassedUnits = 0;
        float sum = 0;
        for (Grade grade: grades) {
            for (Course course: courses) {
                if (grade.code.equals(course.code)) {
                    if (grade.grade > 10) {
                        totalPassedUnits += course.units;
                        sum += course.units * grade.grade;

                        table.append(grade.tableRow());
                    }
                }
            }
        }

        float gpa = sum / totalPassedUnits;

        doc.select("li#std_id").html("Student Id:" + student.id);
        doc.select("li#first_name").html("First Name: " + student.name);
        doc.select("li#last_name").html("Last Name: " + student.secondName);
        doc.select("li#birthdate").html("Birthdate: " + student.birthDate);
        doc.select("li#gpa").html("GPA: " + String.format("%,.2f", gpa));
        doc.select("li#tpu").html("Total Passed Units: " + String.valueOf(totalPassedUnits));

        ctx.status(200);
        ctx.html(doc.html());
    }
}
