package handlers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import io.javalin.http.Context;
import models.Course;
import models.Grade;
import models.Schedule;
import models.Student;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import utils.Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static handlers.Constants.COURSE_TEMPLATE;
import static utils.Constants.SCHEDULES_FILE_PATH;
import static utils.Utils.writeSchedulesListToFile;

public class AddCourseToSchedulePostHandler {
    public static void handle(Context ctx) {
        List<Student> students = Utils.getStudentsFromFile();
        List<Grade> grades = Utils.getStudentGradesFromFile(ctx.formParam("std_id"));
        List<Course> courses = Utils.getCoursesFromFile();
        Course theCourse = null;
        Student theStudent = null;

        for (Student s: students) {
            if (s.id.equals(ctx.formParam("std_id"))) {
                theStudent = s;
                break;
            }
        }

        for (Course item: courses) {
            if (item.code.equals(ctx.pathParam("course_id")) && item.classCode.equals(ctx.pathParam("class_code"))) {
                theCourse = item;
                break;
            }
        }

        if (grades == null || theStudent == null || theCourse == null) {
            ctx.html("Bad request.");
            ctx.status(400);
            return;
        }

        boolean seenFlag = false;
        for (String prerequisite :theCourse.prerequisites) {
            seenFlag = false;
            for (Grade grade :grades) {
                if (grade.code.equals(prerequisite)) {
                    seenFlag = true;
                    if (grade.grade >= 10) {
                        break;
                    } else {
                        ctx.html("Prerequisites not satisfied");
                        return;
                    }
                }
            }
            if (!seenFlag) {
                ctx.html("Prerequisites not satisfied");
                return;
            }
        }

        List<Schedule> allSchedules = new ArrayList<>();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(SCHEDULES_FILE_PATH));
            Type schedulesListType = new TypeToken<ArrayList<Schedule>>(){}.getType();
            allSchedules = gson.fromJson(reader, schedulesListType);
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Schedule> studentSchedules = Utils.getPlanScheduleForStudent(ctx.formParam("std_id"));

        for (Schedule schedule: studentSchedules) {
            for (Course course: courses) {
                if (course.code.equals(schedule.code)) {
                    if (!AddCourseToSchedulePostHandler.classesHaveTimeCollision(theCourse, course)) {
                        ctx.html("Class time collision");
                        return;
                    }
                    if (!AddCourseToSchedulePostHandler.classesHaveExamTimeCollision(theCourse, course)) {
                        ctx.html("Exam time collision");
                        return;
                    }
                }
            }
        }

        Schedule newSchedule = new Schedule();
        newSchedule.StudentId = ctx.formParam("std_id");
        newSchedule.code = theCourse.code;

        allSchedules.add(newSchedule);
        writeSchedulesListToFile(allSchedules);

        ctx.html("Added to schedule");
    }

    public static boolean classesHaveTimeCollision(Course c1, Course c2) {
        boolean day_collision = false;

        for (String d1: c1.classTime.days) {
            for (String d2: c2.classTime.days) {
                if (d1.equals(d2)) {
                    day_collision = true;
                    break;
                }
            }
        }
        if (day_collision) {
            String[] t1 = (c1.classTime.time).split("-");
            String[] t2 = (c2.classTime.time).split("-");
            String s1 = t1[0];
            String e1 = t1[1];
            String s2 = t2[0];
            String e2 = t2[1];
            return (s1.compareTo(s2) < 0 || s1.compareTo(e2) >= 0) && (e1.compareTo(s2) <= 0 || e1.compareTo(e2) > 0);
        }
        return true;
    }

    public static boolean classesHaveExamTimeCollision(Course c1, Course c2) {
        String examTimeStart1 = c1.examTime.start;
        String examTimeEnd1 = c1.examTime.end;
        String examTimeStart2 = c2.examTime.start;
        String examTimeEnd2 = c2.examTime.end;
        return (examTimeStart1.compareTo(examTimeStart2) < 0 || examTimeStart1.compareTo(examTimeEnd2) >= 0) &&
                (examTimeEnd1.compareTo(examTimeStart2) <= 0 || examTimeEnd1.compareTo(examTimeEnd2) > 0);
    }
}
