package handlers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import io.javalin.http.Context;
import models.Course;
import models.Grade;
import models.Schedule;
import models.Student;
import utils.Utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static utils.Constants.SCHEDULES_FILE_PATH;
import static utils.Utils.writeSchedulesListToFile;

public class RemoveCourseFromSchedulePostHandler {
    public static void handle(Context ctx) {
        List<Student> students = Utils.getStudentsFromFile();
        List<Schedule> schedules = Utils.getAlSchedulesFromFile();
        List<Course> courses = Utils.getCoursesFromFile();
        Course theCourse = null;
        Student theStudent = null;
        Schedule theSchedule = null;

        for (Student item: students) {
            if (item.id.equals(ctx.pathParam("student_id"))) {
                theStudent = item;
                break;
            }
        }

        for (Course item: courses) {
            if (item.code.equals(ctx.formParam("course_code")) && item.classCode.equals(ctx.formParam("class_code"))) {
                theCourse = item;
                break;
            }
        }

        for (Schedule item: schedules) {
            if (item.StudentId.equals(ctx.pathParam("student_id")) && item.code.equals(ctx.formParam("course_code"))) {
                theSchedule = item;
                break;
            }
        }

        if (theSchedule == null || theStudent == null || theCourse == null) {
            ctx.html("Bad request.");
            ctx.status(400);
            return;
        }

        schedules.remove(theSchedule);

        writeSchedulesListToFile(schedules);

        ctx.html("Removed from schedule");
    }
}
