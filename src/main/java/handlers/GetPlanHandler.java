package handlers;

import io.javalin.http.Context;
import models.Course;
import models.Schedule;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import utils.Utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GetPlanHandler {
    public static void handle(Context ctx) {
        List<Schedule> studentSchedules = Utils.getPlanScheduleForStudent(ctx.pathParam("student_id"));
        List<Course> courses = Utils.getCoursesFromFile();
        Document doc = null;

        try {
            String fileName = "src/main/resources/templates/plan.html";
            doc = Jsoup.parse(new File(fileName), "utf-8");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e.toString());
        }
        assert doc != null;
        Element table = doc.select("table").first();
        for (Schedule schedule: studentSchedules) {
            Course course = null;
            List<Integer> rows = new ArrayList<>();
            List<Integer> columns = new ArrayList<>();

            for (Course item: courses) {
                if (item.code.equals(schedule.code)) {
                    course = item;
                    break;
                }
            }

            assert course != null;
            if (Arrays.asList(course.classTime.days).contains("Saturday")) {
                rows.add(1);
            }
            if (Arrays.asList(course.classTime.days).contains("Sunday")) {
                rows.add(2);
            }
            if (Arrays.asList(course.classTime.days).contains("Monday")) {
                rows.add(3);
            }
            if (Arrays.asList(course.classTime.days).contains("Tuesday")) {
                rows.add(4);
            }
            if (Arrays.asList(course.classTime.days).contains("Wednesday")) {
                rows.add(5);
            }
            if (course.classTime.time.equals("7:30-9:00")) {
                columns.add(1);
            }
            if (course.classTime.time.equals("9:00-10:30")) {
                columns.add(2);
            }
            if (course.classTime.time.equals("10:30-12:00")) {
                columns.add(3);
            }
            if (course.classTime.time.equals("14:00-15:30")) {
                columns.add(4);
            }
            if (course.classTime.time.equals("16:00-17:30")) {
                columns.add(5);
            }
            for(Integer i: rows) {
                for (Integer j: columns) {
                    try {
                        table.child(0).child(i).child(j).appendText(course.name);
                    } catch (Exception e) {
                        System.out.println(e.toString());
                    }
                }
            }
        }

        ctx.status(200);
        ctx.html(doc.html());
    }
}
