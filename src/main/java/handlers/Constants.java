package handlers;

public class Constants {
    public static final String PROFILE_TEMPLATE = "src/main/resources/templates/profile.html";
    public static final String COURSE_TEMPLATE = "src/main/resources/templates/course.html";
    public static final String CHANGE_PLAN_TEMPLATE = "src/main/resources/templates/change_plan.html";
    public static final String SUBMIT_TEMPLATE = "src/main/resources/templates/submit.html";
    public static final String SUBMIT_OK_TEMPLATE = "src/main/resources/templates/submit_ok.html";
    public static final String SUBMIT_FAILED_TEMPLATE = "src/main/resources/templates/submit_failed.html";
}
