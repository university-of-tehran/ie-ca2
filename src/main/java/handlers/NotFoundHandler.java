package handlers;

import java.io.File;
import java.io.IOException;
import io.javalin.http.Context;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class NotFoundHandler {
    public static void handle(Context ctx) throws IOException {
        String fileName = "src/main/resources/templates/404.html";
        Document doc = Jsoup.parse(new File(fileName), "utf-8");
        ctx.html(doc.html());
    }
}
