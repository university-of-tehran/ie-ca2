package handlers;

import io.javalin.http.Context;
import models.Course;
import models.Schedule;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import utils.Utils;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static handlers.Constants.SUBMIT_TEMPLATE;

public class SubmitGetHandler {
    public static void handle(Context ctx) {
        String studentId = ctx.pathParam("student_id");

        List<Schedule> studentSchedules = Utils.getPlanScheduleForStudent(studentId);
        List<Course> courses = Utils.getCoursesFromFile();
        Document doc = null;

        try {
            doc = Jsoup.parse(new File(SUBMIT_TEMPLATE), "utf-8");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e.toString());
        }
        assert doc != null;

        int totalUnits = 0;
        for (Schedule item :studentSchedules) {
            for (Course course: courses) {
                if (item.code.equals(course.code)) {
                    totalUnits += course.units;
                }
            }
        }

        doc.select("li#code").html("Student Id: " + studentId);
        doc.select("li#units").html("Total Units: " + totalUnits);

        Element form = doc.select("form").first();
        form.attr("action", "/submit");
        form.append("<input name=\"std_id\" value=\"" + studentId + "\" type=\"hidden\">");

        ctx.html(doc.html());
    }
}
