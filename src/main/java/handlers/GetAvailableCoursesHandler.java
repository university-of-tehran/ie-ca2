package handlers;

import java.io.File;
import java.io.IOException;
import java.util.List;
import io.javalin.http.Context;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import utils.Utils;
import models.Course;

public class GetAvailableCoursesHandler {
    public static void handle(Context ctx) {
        List<Course> courses = Utils.getCoursesFromFile();

        Document doc = null;

        try {
            String fileName = "src/main/resources/templates/courses.html";
            doc = Jsoup.parse(new File(fileName), "utf-8");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e.toString());
        }

        assert doc != null;
        Element table = doc.select("table").first();

        for (Course course: courses) {
            table.append(course.tableRow());
        }

        ctx.status(200);
        ctx.html(doc.html());
    }
}
