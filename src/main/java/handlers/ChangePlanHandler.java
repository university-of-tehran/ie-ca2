package handlers;

import io.javalin.http.Context;
import models.Course;
import models.Schedule;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import utils.Utils;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static handlers.Constants.CHANGE_PLAN_TEMPLATE;

public class ChangePlanHandler {
    public static void handle(Context ctx) {
        List<Schedule> studentSchedules = Utils.getPlanScheduleForStudent(ctx.pathParam("student_id"));
        List<Course> courses = Utils.getCoursesFromFile();
        Course theCourse = null;
        Document doc = null;

        try {
            doc = Jsoup.parse(new File(CHANGE_PLAN_TEMPLATE), "utf-8");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e.toString());
        }
        assert doc != null;

        try {
            Element table = doc.select("table").first();
            for (Schedule item :studentSchedules) {
                for (Course course: courses) {
                    if (item.code.equals(course.code)) {
                        theCourse = course;
                        break;
                    }
                }
                assert theCourse != null;
                table.append(item.tableRow(theCourse));
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        ctx.html(doc.html());
    }
}
