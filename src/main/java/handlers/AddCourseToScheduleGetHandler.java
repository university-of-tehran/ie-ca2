package handlers;

import io.javalin.http.Context;
import models.Course;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import utils.Utils;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static handlers.Constants.COURSE_TEMPLATE;

public class AddCourseToScheduleGetHandler {
    public static void handle(Context ctx) {
        List<Course> courses = Utils.getCoursesFromFile();
        Course theCourse = null;
        Document doc = null;

        try {
            doc = Jsoup.parse(new File(COURSE_TEMPLATE), "utf-8");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e.toString());
        }
        assert doc != null;

        for (Course item: courses) {
            if (item.code.equals(ctx.pathParam("course_id")) && item.classCode.equals(ctx.pathParam("class_code"))) {
                theCourse = item;
                break;
            }
        }
        assert theCourse != null;

        try {
            Element uList = doc.select("ul").first();
            uList.child(0).text("Code: " + theCourse.code);
            uList.child(1).text("Class Code: " + theCourse.classCode);
            uList.child(2).text("units: " + theCourse.units);
            uList.child(3).text("Days: " + String.join(", ", theCourse.classTime.days));
            uList.child(4).text("Time: " + theCourse.classTime.time);
            Element form = doc.select("form").first();
            form.attr("action", "/course/" + theCourse.code + "/" + theCourse.classCode);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        ctx.html(doc.html());
    }
}
