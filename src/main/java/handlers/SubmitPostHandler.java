package handlers;

import io.javalin.http.Context;
import models.Course;
import models.Grade;
import models.Schedule;
import models.Student;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import utils.Utils;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static handlers.Constants.*;

public class SubmitPostHandler {
    public static void handle(Context ctx) {
        String studentId = ctx.formParam("std_id");
        List<Course> courses = Utils.getCoursesFromFile();
        List<Schedule> studentSchedules = Utils.getPlanScheduleForStudent(studentId);

        Document sumbit_ok_doc = null;
        Document sumbit_failed_doc = null;
        try {
            sumbit_ok_doc = Jsoup.parse(new File(SUBMIT_OK_TEMPLATE), "utf-8");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e.toString());
        }
        assert sumbit_ok_doc != null;

        try {
            sumbit_failed_doc = Jsoup.parse(new File(SUBMIT_FAILED_TEMPLATE), "utf-8");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e.toString());
        }
        assert sumbit_failed_doc != null;

        int totalUnits = 0;
        for (Schedule item :studentSchedules) {
            for (Course course: courses) {
                if (item.code.equals(course.code)) {
                    totalUnits += course.units;
                }
            }
        }

        if (totalUnits > 20 || totalUnits < 12) {
            ctx.status(400);
            ctx.html(sumbit_failed_doc.html());
        }
        else {
            ctx.status(200);
            ctx.html(sumbit_ok_doc.html());
        }
    }
}
