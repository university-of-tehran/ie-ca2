package server;

import io.javalin.Javalin;
import clients.BolbolestanClient;
import handlers.*;

public class Server {
    public static void main(String[] args) {
        BolbolestanClient.loadData();

        Javalin app = Javalin.create().start(8080);
        app.get("/courses", GetAvailableCoursesHandler::handle);
        app.get("/profile/:student_id", GetProfileDetailsHandler::handle);
        app.post("/course/remove/:student_id", RemoveCourseFromSchedulePostHandler::handle);
        app.get("/course/:course_id/:class_code", AddCourseToScheduleGetHandler::handle);
        app.post("/course/:course_id/:class_code", AddCourseToSchedulePostHandler::handle);
        app.get("/change_plan/:student_id", ChangePlanHandler::handle);
        app.get("/plan/:student_id", GetPlanHandler::handle);
        app.get("/submit/:student_id", SubmitGetHandler::handle);
        app.post("/submit", SubmitPostHandler::handle);
        app.get("*", NotFoundHandler::handle);
    }
}
