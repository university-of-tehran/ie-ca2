package utils;

public class Constants {
    public static final String COURSES_FILE_PATH = "src/main/resources/data/courses.json";
    public static final String STUDENTS_FILE_PATH = "src/main/resources/data/students.json";
    public static final String GRADES_FILE_PATH = "src/main/resources/data/grades.json";
    public static final String SCHEDULES_FILE_PATH = "src/main/resources/data/schedules.json";
}
