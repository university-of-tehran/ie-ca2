package models;

public class Schedule {
    public String StudentId;
    public String code;
    public String classCode;
    public String status = "non-finalized";

    public String tableRow(Course course) {
        assert this.code.equals(course.code);
        return "<tr>\n" +
                "            <td>" + course.code + "</td>\n" +
                "            <td>" + course.classCode + "</td> \n" +
                "            <td>" + course.name + "</td>\n" +
                "            <td>" + course.units + "</td>\n" +
                "            <td>        \n" +
                "                <form action=\"" + "/course/remove/" + this.StudentId + "\" method=\"POST\" >\n" +
                "                    <input id=\"form_course_code\" type=\"hidden\" name=\"course_code\" value=\"" + course.code + "\">\n" +
                "                    <input id=\"form_class_code\" type=\"hidden\" name=\"class_code\" value=\"" + course.classCode + "\">\n" +
                "                    <button type=\"submit\">Remove</button>\n" +
                "                </form>\n" +
                "            </td>\n" +
                "        </tr>";
    }
}
