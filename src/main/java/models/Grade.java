package models;

public class Grade {
    public String code;
    public float grade;

    public String tableRow() {
        return "       <tr>\n" +
                "            <td>" + this.code + "</td>\n" +
                "            <td>" + this.grade + "</td> \n" +
                "        </tr>\n";
    }
}
