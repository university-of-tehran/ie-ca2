package models;

public class Course {
    public String code;
    public String classCode;
    public String name;
    public String type;
    public String Instructor;
    public int units;
    public ClassTime classTime;
    public ExamTime examTime;
    public int capacity;
    public String[] prerequisites;

    public String tableRow() {
        return "       <tr>\n" +
                "            <td>" + this.code + "</td>\n" +
                "            <td>" + this.classCode + "</td> \n" +
                "            <td>" + this.name + "</td>\n" +
                "            <td>" + this.units + "</td>\n" +
                "            <td>" + this.capacity + "</td>\n" +
                "            <td>" + this.type + "</td>\n" +
                "            <td>" + String.join("|", this.classTime.days) + "</td>\n" +
                "            <td>" + this.classTime.time + "</td>\n" +
                "            <td>" + this.examTime.start + "</td>\n" +
                "            <td>" + this.examTime.end + "</td>\n" +
                "            <td>" + String.join("|", this.prerequisites) + "</td>\n" +
                "            <td><a href=\"/course/" + this.code + "/" + this.classCode + "\">Link</a></td>\n" +
                "        </tr>\n";
    }
}
