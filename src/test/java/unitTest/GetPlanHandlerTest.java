package unitTest;

import handlers.*;
import io.javalin.http.Context;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static utils.Constants.*;

public class GetPlanHandlerTest {

    private Context ctx = mock(Context.class);

    @Before
    public void setUpStreams() {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(SCHEDULES_FILE_PATH);
            writer.print("[{\"StudentId\":\"810196285\",\"code\":\"8101001\",\"status\":\"non-finalized\"}]");
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void CheckGetPlanHandlerResponse() {
        when(ctx.pathParam("student_id")).thenReturn("810196285");
        GetPlanHandler.handle(ctx);
        verify(ctx).status(200);
        verify(ctx).html(argThat(s -> s.contains(" <tr> \n" +
                "     <td>Tuesday</td> \n" +
                "     <td></td> \n" +
                "     <td></td> \n" +
                "     <td>Advanced Programming</td> \n" +
                "     <td></td> \n" +
                "     <td></td> \n" +
                "    </tr> \n") && s.contains("    <tr> \n" +
                "     <td>Sunday</td> \n" +
                "     <td></td> \n" +
                "     <td></td> \n" +
                "     <td>Advanced Programming</td> \n" +
                "     <td></td> \n" +
                "     <td></td> \n" +
                "    </tr> ")));
    }
}