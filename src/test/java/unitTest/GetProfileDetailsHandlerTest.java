package unitTest;

import handlers.*;
import io.javalin.http.Context;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static utils.Constants.*;

public class GetProfileDetailsHandlerTest {

    private Context ctx = mock(Context.class);

    @Before
    public void setUpStreams() {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(COURSES_FILE_PATH);
            writer.print("[{\"code\": \"8101001\", \"classCode\": \"01\", \"name\": \"Advanced Programming\", \"units\": 3, \"type\": \"Asli\", \"instructor\": \"sample\", \"capacity\": 45, \"prerequisites\": [\"8101013\"], \"classTime\": {\"days\": [\"Sunday\", \"Tuesday\"], \"time\": \"10:30-12:00\"}, \"examTime\": {\"start\": \"2021-06-21T14:00:00\", \"end\": \"2021-06-21T17:00:00\"}}]");
            writer.close();
            writer = new PrintWriter(STUDENTS_FILE_PATH);
            writer.print("[{\"id\": \"810196285\", \"name\": \"Sina\", \"secondName\": \"Mohammadian\", \"birthDate\": \"1378/08/04\"}]");
            writer.close();
            writer = new PrintWriter(GRADES_FILE_PATH);
            writer.print("{\"810196285\":[{\"code\":\"8101001\",\"grade\":18.0},{\"code\":\"8101004\",\"grade\":9.0}]}");
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void CheckGetProfileDetailsHandlerResponse() {
        when(ctx.pathParam("student_id")).thenReturn("810196285");
        GetProfileDetailsHandler.handle(ctx);
        verify(ctx).status(200);
        verify(ctx).html(argThat(s -> s.contains("    <tr> \n" +
                "     <td>8101001</td> \n" +
                "     <td>18.0</td> \n" +
                "    </tr> \n")));
    }

    @Test
    public void CheckNotFoundResponse() {
        when(ctx.pathParam("student_id")).thenReturn("invalid_id");
        GetProfileDetailsHandler.handle(ctx);
        verify(ctx).status(404);
    }
}