package unitTest;

import handlers.*;
import io.javalin.http.Context;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static utils.Constants.*;

public class getAvailableCoursesHandlerTest {

    private Context ctx = mock(Context.class);

    @Before
    public void setUpStreams() {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(COURSES_FILE_PATH);
            writer.print("[{\"code\": \"8101001\", \"classCode\": \"01\", \"name\": \"Advanced Programming\", \"units\": 3, \"type\": \"Asli\", \"instructor\": \"sample\", \"capacity\": 45, \"prerequisites\": [\"8101013\"], \"classTime\": {\"days\": [\"Sunday\", \"Tuesday\"], \"time\": \"10:30-12:00\"}, \"examTime\": {\"start\": \"2021-06-21T14:00:00\", \"end\": \"2021-06-21T17:00:00\"}}]");
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void CheckGetAvailableCoursesHandlerResponse() {
        GetAvailableCoursesHandler.handle(ctx);
        verify(ctx).status(200);
        verify(ctx).html(argThat(s -> s.contains("<tbody>\n" +
                "    <tr> \n" +
                "     <td>8101001</td> \n" +
                "     <td>01</td> \n" +
                "     <td>Advanced Programming</td> \n" +
                "     <td>3</td> \n" +
                "     <td>45</td> \n" +
                "     <td>Asli</td> \n" +
                "     <td>Sunday|Tuesday</td> \n" +
                "     <td>10:30-12:00</td> \n" +
                "     <td>2021-06-21T14:00:00</td> \n" +
                "     <td>2021-06-21T17:00:00</td> \n" +
                "     <td>8101013</td> \n" +
                "     <td><a href=\"/course/8101001/01\">Link</a></td> \n" +
                "    </tr> \n" +
                "   </tbody>\n")));
    }
}