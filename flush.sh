rm -rf target
rm ./src/main/resources/data/courses.json
rm ./src/main/resources/data/schedules.json
rm ./src/main/resources/data/students.json
rm ./src/main/resources/data/grades.json
touch ./src/main/resources/data/courses.json
touch ./src/main/resources/data/schedules.json
touch ./src/main/resources/data/students.json
touch ./src/main/resources/data/grades.json
echo "[]" >> ./src/main/resources/data/courses.json
echo "[]" >> ./src/main/resources/data/schedules.json
echo "[]" >> ./src/main/resources/data/students.json
echo "{}" >> ./src/main/resources/data/grades.json
